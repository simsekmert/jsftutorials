package managedbeans;

public class Person {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String welcomeUser(){
		if(isEmpty())
			return "error-page";
		else
			return "greetings";
	}
	
	private boolean isEmpty(){
		return name.equals("");
	}
}
