package somepackage;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "navi")
public class Navigator {
	private String[] pages = {"page1","page2","page3"};
	
	public String choosePage(){
		return pages[(int)(Math.random()*3)];
	}
	
}
